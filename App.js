/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import { client } from './src/config';
import Home from './src/screen/Home';
export default class App extends Component {
	render() {
		return (
			<ApolloProvider client={client}>
				<View style={styles.container}>
					<Text style={styles.welcome}>I am a receiver</Text>
					<Home />
				</View>
			</ApolloProvider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5
	}
});
