export const SERVER_URL = 'http://192.168.2.111:4000/';
export const DELIVERY_NOTIFICATION = {
	ANDROID: {
		ACCEPT_ACTION: 'accept',
		DO_NOT_ACCEPT_ACTION: 'do_not_accept',
		DO_NOT_ACCEPT_REASON: 'do_not_accept_reason',
	},
};
