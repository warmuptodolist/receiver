import { HttpLink } from 'apollo-link-http';
import { SERVER_URL } from '.';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';
const link = new HttpLink({ uri: SERVER_URL });
console.log(link);
export const client = new ApolloClient({
	link,
	cache: new InMemoryCache()
});
