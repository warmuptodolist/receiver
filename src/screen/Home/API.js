import { client } from '../../config';
import gql from 'graphql-tag';

const uploadClientFcmToken = async (id, token) => {
	try {
		let UPDATE_FCM = gql`
			mutation UpdateFCMToken($id: String!, $token: String!) {
				updateFCMToken(id: $id, token: $token) {
					id
					name
					token
				}
			}
		`;

		let result = await client.mutate({
			mutation: UPDATE_FCM,
			variables: { id, token },
		});

		return result;
	} catch (e) {
		console.log('error in uploadClientFcmToken', JSON.stringify(e));
		throw e;
	}
};

const giveDontAcceptReason = (reason) => {
	let GIVE_REJECT_REASON = gql`
		mutation GiveRejectedReason($reason: String!) {
			giveRejectedReason(reason: $reason)
		}
	`;

	return client.mutate({
		mutation: GIVE_REJECT_REASON,
		variables: { reason },
	});
};

const acceptItemDelivering = () => {
	let ACCEPT_ITEM = gql`
		mutation ItemIsAccepted {
			itemIsAccepted
		}
	`;

	return client.mutate({
		mutation: ACCEPT_ITEM,
	});
};

export { uploadClientFcmToken, giveDontAcceptReason, acceptItemDelivering };
