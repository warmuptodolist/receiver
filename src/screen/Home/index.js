import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import _ from 'lodash';
import firebase from 'react-native-firebase';
import { uploadClientFcmToken, giveDontAcceptReason, acceptItemDelivering } from './API';
import { DELIVERY_NOTIFICATION } from '../../config';

const GET_USER = gql`
	query GetUserByID($userId: ID!) {
		getUserByID(userId: $userId) {
			id
			name
		}
	}
`;

const USER_ID = '5c0b9fa824aa9a0009b1649d';
export default class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	async componentDidMount() {
		try {
			const fcmToken = await firebase.messaging().getToken();
			let response = await uploadClientFcmToken(USER_ID, fcmToken);

			firebase.notifications().onNotification(this.handleNotification);
			firebase.notifications().onNotificationOpened(this.handleNotificationOpened);
		} catch (e) {
			console.log('error in component did mount ===>', e);
		}
	}

	handleNotificationOpened = async (notification) => {
		const { action, results } = notification;

		if (action == DELIVERY_NOTIFICATION.ANDROID.DO_NOT_ACCEPT_ACTION) {
			try {
				let response = await giveDontAcceptReason(results[DELIVERY_NOTIFICATION.ANDROID.DO_NOT_ACCEPT_REASON]);
				console.log('response ===>', response);
			} catch (e) {
				console.log('error ====>', e);
			}
		} else if (action == DELIVERY_NOTIFICATION.ANDROID.ACCEPT_ACTION) {
			try {
				let response = await acceptItemDelivering();
				console.log('response ======>', response);
			} catch (e) {
				console.log('error ====>', e);
			}
		}
	};
	handleNotification = (notification) => {
		console.log('this is notification ====>', notification);
		let notificationTitle = notification._title;
		let notificationBody = notification._body;
		let notificationData = notification._data;

		///create Channel for android notificaion
		const channel = new firebase.notifications.Android.Channel(
			'joonaak_channel',
			'Joonaak_Channel',
			firebase.notifications.Android.Importance.Max,
		).setDescription('My apps test channel');

		// Create the channel
		firebase.notifications().android.createChannel(channel);
		const myNotification = new firebase.notifications.Notification()
			.setNotificationId('notificationId')
			.setTitle(notificationTitle)
			.setBody(notificationBody)
			.setData({
				...notificationData,
			});

		///create Action for android Notification
		let androidAction = new firebase.notifications.Android.Action(
			DELIVERY_NOTIFICATION.ANDROID.ACCEPT_ACTION,
			'ic_launcher',
			'Accept',
		);
		let androidAction2 = new firebase.notifications.Android.Action(
			DELIVERY_NOTIFICATION.ANDROID.DO_NOT_ACCEPT_ACTION,
			'ic_launcher',
			"Don't Accept",
		);

		//create Input for android notification
		let androidAction2RemoteInput = new firebase.notifications.Android.RemoteInput(
			DELIVERY_NOTIFICATION.ANDROID.DO_NOT_ACCEPT_REASON, //key to get reason of why don't accept
		);

		androidAction2RemoteInput.setAllowFreeFormInput(true);

		androidAction2.addRemoteInput(androidAction2RemoteInput);
		myNotification.android
			.setChannelId('joonaak_channel')
			.android.setSmallIcon('ic_launcher')
			.android.addAction(androidAction)
			.android.addAction(androidAction2);

		firebase.notifications().displayNotification(myNotification);
	};

	render() {
		return (
			<View>
				<Query query={GET_USER} variables={{ userId: USER_ID }}>
					{({ loading, error, data }) => {
						let err = JSON.stringify(error);
						if (err) {
						}
						console.log('data ====>', _.get(data, 'getUserByID.name'));
						return <Text>Username is {_.get(data, 'getUserByID.name')}</Text>;
					}}
				</Query>
			</View>
		);
	}
}
